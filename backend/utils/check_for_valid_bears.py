from bears.models import Bear


def check_for_valid_bears(bears):
    valid_bear_tuple = Bear.objects.all().values_list('name')

    # Converting tuples in queryset to strings
    valid_bear_list = []
    for b in valid_bear_tuple:
        valid_bear_list.append(b[0])

    # Verifying user supplied bears
    for bear in bears:
        if bear not in valid_bear_list:
            return False

    return True
